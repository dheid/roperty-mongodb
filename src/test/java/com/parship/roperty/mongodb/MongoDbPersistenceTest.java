package com.parship.roperty.mongodb;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.parship.roperty.DomainSpecificValue;
import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValues;
import com.parship.roperty.KeyValuesFactory;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.parship.roperty.mongodb.RopertyMongoDbAttribute.VALUE;
import static com.parship.roperty.mongodb.RopertyMongoDbAttribute.VALUES;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MongoDbPersistenceTest {

    public static final String DESCRIPTION = "description";
    public static final long NUMBER_OF_DOCUMENTS = 1L;
    public static final String CHANGE_SET = "changeSet";
    public static final String PATTERN = "pattern";
    private static final String KEY = "key";

    @InjectMocks
    private MongoDbPersistence mongoDbPersistence;

    @Mock
    private KeyValuesFactory keyValuesFactory;

    @Mock
    private DomainSpecificValueFactory domainSpecificValueFactory;

    @Mock
    private CollectionProvider collectionProvider;

    @Mock
    private KeyDocumentDAO keyDocumentDAO;

    @Mock
    private Document keyDocument;

    @Mock
    private ValueDocumentDAO valueDocumentDAO;

    @Mock
    private Document valueDocument;

    @Mock
    private ValueDocumentTransformerFactory valueDocumentTransformerFactory;

    @Mock
    private ValueDocumentTransformer valueDocumentTransformer;

    @Mock
    private KeyValues keyValues;

    @Mock
    private MongoCollection<Document> collection;

    @Mock
    private KeyBlockFactory keyBlockFactory;

    @Mock
    private KeyBlock keyBlock;

    @Mock
    private FindIterable<Document> allKeys;

    @Mock
    private DomainSpecificValue domainSpecificValue;

    @Mock
    private ValueAnalyzerFactory valueAnalyzerFactory;

    @Mock
    private ValueAnalyzer valueAnalyzer;

    @Mock
    private Object value;

    @Mock
    private RemoveOperationFactory removeOperationFactory;

    @Mock
    private RemoveOperation removeOperation;

    private List<Document> values;

    private Map<String, KeyValues> keyValuesMap;

    private Set<DomainSpecificValue> domainSpecificValues;

    @Before
    public void initializeCollections() {
        values = Collections.singletonList(valueDocument);
        keyValuesMap = Collections.singletonMap(KEY, keyValues);
        domainSpecificValues = new HashSet<>(Arrays.asList(domainSpecificValue));
    }

    @Test(expected = NullPointerException.class)
    public void loadWithoutKeyShouldFail() {
        mongoDbPersistence.load(null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void loadWithoutKeyValuesFactoryShouldFail() {
        mongoDbPersistence.load(KEY, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void loadWithoutDomainSpecificValueFactoryShouldFail() {
        mongoDbPersistence.load(KEY, keyValuesFactory, null);
    }

    @Test(expected = NullPointerException.class)
    public void loadWithoutCollectionProviderShouldFail() {
        mongoDbPersistence.setCollectionProvider(null);
        mongoDbPersistence.load(KEY, keyValuesFactory, domainSpecificValueFactory);
    }

    @Test(expected = NullPointerException.class)
    public void loadWithoutKeyDAOShouldFail() {
        mongoDbPersistence.setKeyDocumentDAO(null);
        mongoDbPersistence.load(KEY, keyValuesFactory, domainSpecificValueFactory);
    }


    @Test
    public void loadReturnsNullIfKeyNotFound() {
        KeyValues result = mongoDbPersistence.load(KEY, keyValuesFactory, domainSpecificValueFactory);
        verify(keyDocumentDAO).findKey(KEY);
        assertThat(result, nullValue());
    }

    @Test(expected = NullPointerException.class)
    public void loadWithoutValueDocumentDAOFails() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        mongoDbPersistence.setValueDocumentDAO(null);

        mongoDbPersistence.load(KEY, keyValuesFactory, domainSpecificValueFactory);

        verify(keyDocumentDAO).findKey(KEY);
    }

    @Test(expected = NullPointerException.class)
    public void loadWithoutValueDocumentTransformerFactoryShouldFail() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(valueDocumentDAO.getValues(keyDocument)).thenReturn(values);
        mongoDbPersistence.setValueDocumentTransformerFactory(null);

        mongoDbPersistence.load(KEY, keyValuesFactory, domainSpecificValueFactory);

        verify(keyDocumentDAO).findKey(KEY);
        verify(valueDocumentDAO).getValues(keyDocument);
    }

    @Test
    public void load() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(valueDocumentDAO.getValues(keyDocument)).thenReturn(values);
        when(valueDocumentTransformerFactory.create(keyValuesFactory, domainSpecificValueFactory))
            .thenReturn(valueDocumentTransformer);
        when(keyDocument.getString(RopertyMongoDbAttribute.DESCRIPTION.getName())).thenReturn(DESCRIPTION);
        when(valueDocumentTransformer.transform(values, DESCRIPTION)).thenReturn(keyValues);

        KeyValues result = mongoDbPersistence.load(KEY, keyValuesFactory, domainSpecificValueFactory);

        verify(keyDocumentDAO).findKey(KEY);
        verify(valueDocumentDAO).getValues(keyDocument);
        verify(keyDocument).getString(RopertyMongoDbAttribute.DESCRIPTION.getName());
        verify(valueDocumentTransformerFactory).create(keyValuesFactory, domainSpecificValueFactory);
        verify(valueDocumentTransformer).transform(values, DESCRIPTION);
        assertThat(result, is(keyValues));
    }

    @Test(expected = NullPointerException.class)
    public void loadAllWithNullKeyValuesFactoryShouldFail() {
        mongoDbPersistence.loadAll(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void loadAllWithNullDomainSpecificValuesFactoryShouldFail() {
        mongoDbPersistence.loadAll(keyValuesFactory, null);
    }

    @Test(expected = NullPointerException.class)
    public void loadAllWithoutCollectionProviderShouldFail() {
        mongoDbPersistence.setCollectionProvider(null);
        mongoDbPersistence.loadAll(keyValuesFactory, domainSpecificValueFactory);
    }

    @Test(expected = NullPointerException.class)
    public void loadAllWithoutKeyBlockFactoryShouldFail() {
        mongoDbPersistence.setKeyBlockFactory(null);
        mongoDbPersistence.loadAll(keyValuesFactory, domainSpecificValueFactory);
    }

    @Test(expected = NullPointerException.class)
    public void loadAllWithoutKeyDocumentDAOShouldFail() {
        mongoDbPersistence.setKeyDocumentDAO(null);
        mongoDbPersistence.loadAll(keyValuesFactory, domainSpecificValueFactory);
    }

    @Test
    public void loadAllShouldReturnKeyValuesFromKeyBlock() {
        when(collectionProvider.getCollection()).thenReturn(collection);
        when(collection.count()).thenReturn(NUMBER_OF_DOCUMENTS);
        when(keyBlockFactory.createKeyBlock((int) NUMBER_OF_DOCUMENTS, keyValuesFactory, domainSpecificValueFactory))
            .thenReturn(keyBlock);
        when(keyDocumentDAO.findAllKeys()).thenReturn(allKeys);
        when(keyBlock.getResult()).thenReturn(keyValuesMap);

        Map<String, KeyValues> result = mongoDbPersistence.loadAll(keyValuesFactory, domainSpecificValueFactory);

        verify(collectionProvider).getCollection();
        verify(collection).count();
        verify(keyBlockFactory).createKeyBlock((int) NUMBER_OF_DOCUMENTS, keyValuesFactory, domainSpecificValueFactory);
        verify(keyDocumentDAO).findAllKeys();
        verify(allKeys).forEach(keyBlock);
        verify(keyBlock).getResult();
        assertThat(result, hasEntry(KEY, keyValues));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void loadAllShouldReturnUnmodifiableMap() {
        when(collectionProvider.getCollection()).thenReturn(collection);
        when(collection.count()).thenReturn(NUMBER_OF_DOCUMENTS);
        when(keyBlockFactory.createKeyBlock((int) NUMBER_OF_DOCUMENTS, keyValuesFactory, domainSpecificValueFactory))
            .thenReturn(keyBlock);
        when(keyDocumentDAO.findAllKeys()).thenReturn(allKeys);
        when(keyBlock.getResult()).thenReturn(keyValuesMap);

        Map<String, KeyValues> result = mongoDbPersistence.loadAll(keyValuesFactory, domainSpecificValueFactory);
        result.put(KEY, keyValues);
    }

    @Test(expected = NullPointerException.class)
    public void reloadShouldFailIfNullKeyValuesMapGiven() {
        mongoDbPersistence.reload(null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void reloadShouldFailIfNoKeyValuesFactoryGiven() {
        mongoDbPersistence.reload(keyValuesMap, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void reloadShouldFailIfNoDomainSpecificValuesFactoryGiven() {
        mongoDbPersistence.reload(keyValuesMap, keyValuesFactory, null);
    }

    @Test
    public void reloadShouldNotReturnUnknownKey() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(null);

        Map<String, KeyValues> result = mongoDbPersistence.reload(keyValuesMap, keyValuesFactory, domainSpecificValueFactory);

        verify(keyDocumentDAO).findKey(KEY);
        assertThat(result.isEmpty(), is(true));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void reloadShouldReturnUnmodifiableMap() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(null);

        Map<String, KeyValues> result = mongoDbPersistence.reload(keyValuesMap, keyValuesFactory, domainSpecificValueFactory);
        result.put(KEY, keyValues);
    }

    @Test
    public void reloadWithEmptyKeyValuesMapShouldDoNothing() {
        Map<String, KeyValues> keyValuesMap = Collections.emptyMap();
        mongoDbPersistence.reload(keyValuesMap, keyValuesFactory, domainSpecificValueFactory);
        verifyZeroInteractions(keyValuesFactory, domainSpecificValueFactory, collectionProvider, keyDocumentDAO,
            keyDocument, valueDocumentDAO, valueDocument, valueDocumentTransformerFactory, valueDocumentTransformer,
            keyValues, collection, keyBlockFactory, keyBlock, allKeys, domainSpecificValue, valueAnalyzerFactory,
            removeOperationFactory, removeOperation);
    }

    @Test
    public void reload() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(valueDocumentTransformerFactory.create(keyValuesFactory, domainSpecificValueFactory))
            .thenReturn(valueDocumentTransformer);
        when(valueDocumentTransformer.transform(values, DESCRIPTION)).thenReturn(keyValues);
        when(valueDocumentDAO.getValues(keyDocument)).thenReturn(values);
        when(keyValues.getDescription()).thenReturn(DESCRIPTION);

        Map<String, KeyValues> result = mongoDbPersistence.reload(keyValuesMap, keyValuesFactory, domainSpecificValueFactory);

        verify(keyDocumentDAO).findKey(KEY);
        verify(valueDocumentDAO).getValues(keyDocument);
        verify(valueDocumentTransformerFactory).create(keyValuesFactory, domainSpecificValueFactory);
        verify(valueDocumentTransformer).transform(values, DESCRIPTION);
        verify(keyValues).getDescription();
        assertThat(result, hasEntry(KEY, keyValues));
    }

    @Test(expected = NullPointerException.class)
    public void storeNullKeyShouldFail() {
        mongoDbPersistence.store(null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void storeEmptyStringAsKeyFails() {
        mongoDbPersistence.store("", null, null);
    }

    @Test(expected = NullPointerException.class)
    public void storeWithNullKeyValuesFails() {
        mongoDbPersistence.store(KEY, null, null);
    }

    @Test
    public void storeEmptyMapShouldDoNothing() {
        when(keyValues.getDomainSpecificValues()).thenReturn(Collections.<DomainSpecificValue>emptySet());
        mongoDbPersistence.store(KEY, keyValues, null);
        verify(keyValues).getDomainSpecificValues();
        verifyZeroInteractions(keyValuesFactory, domainSpecificValueFactory, collectionProvider, keyDocumentDAO,
            keyDocument, valueDocumentDAO, valueDocument, valueDocumentTransformerFactory, valueDocumentTransformer,
            keyValues, collection, keyBlockFactory, keyBlock, allKeys, domainSpecificValue, valueAnalyzerFactory,
            removeOperationFactory, removeOperation);
    }

    @Test
    public void storeExistingKeyWithNullChangeSetShouldChangeExistingValueDocument() {
        when(keyValues.getDomainSpecificValues()).thenReturn(domainSpecificValues);
        when(collectionProvider.getCollection()).thenReturn(collection);
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(valueDocumentDAO.getValues(keyDocument)).thenReturn(values);
        when(domainSpecificValue.changeSetIs(null)).thenReturn(true);
        when(domainSpecificValue.getValue()).thenReturn(value);
        when(valueAnalyzerFactory.createValueAnalyzer(domainSpecificValue)).thenReturn(valueAnalyzer);
        when(valueAnalyzer.samePatternAndChangeSetAs(valueDocument)).thenReturn(true);

        mongoDbPersistence.store(KEY, keyValues, null);

        verify(keyValues).getDomainSpecificValues();
        verify(collectionProvider).getCollection();
        verify(keyDocumentDAO).findKey(KEY);
        verify(keyDocument).put(VALUES.getName(), values);
        verify(valueDocumentDAO).getValues(keyDocument);
        verify(collection).updateOne(any(Bson.class), eq(new Document("$set", keyDocument)));
        verify(domainSpecificValue).changeSetIs(null);
        verify(domainSpecificValue).getValue();
        verify(valueAnalyzerFactory).createValueAnalyzer(domainSpecificValue);
        verify(valueAnalyzer).samePatternAndChangeSetAs(valueDocument);
        verify(valueDocument).put(VALUE.getName(), value);
    }

    @Test
    public void storeExistingKeyWithEmptyChangeSetShouldChangeExistingValueDocument() {
        when(keyValues.getDomainSpecificValues()).thenReturn(domainSpecificValues);
        when(collectionProvider.getCollection()).thenReturn(collection);
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(valueDocumentDAO.getValues(keyDocument)).thenReturn(values);
        when(domainSpecificValue.changeSetIs(null)).thenReturn(true);
        when(domainSpecificValue.getValue()).thenReturn(value);
        when(valueAnalyzerFactory.createValueAnalyzer(domainSpecificValue)).thenReturn(valueAnalyzer);
        when(valueAnalyzer.samePatternAndChangeSetAs(valueDocument)).thenReturn(true);

        mongoDbPersistence.store(KEY, keyValues, "");

        verify(keyValues).getDomainSpecificValues();
        verify(collectionProvider).getCollection();
        verify(keyDocumentDAO).findKey(KEY);
        verify(keyDocument).put(VALUES.getName(), values);
        verify(valueDocumentDAO).getValues(keyDocument);
        verify(collection).updateOne(any(Bson.class), eq(new Document("$set", keyDocument)));
        verify(domainSpecificValue).changeSetIs(null);
        verify(domainSpecificValue).getValue();
        verify(valueAnalyzerFactory).createValueAnalyzer(domainSpecificValue);
        verify(valueAnalyzer).samePatternAndChangeSetAs(valueDocument);
        verify(valueDocument).put(VALUE.getName(), value);
    }

    @Test
    public void storeExistingKeyWithChangeSetShouldCreateNewValueDocument() {
        when(keyValues.getDomainSpecificValues()).thenReturn(domainSpecificValues);
        when(collectionProvider.getCollection()).thenReturn(collection);
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(valueDocumentDAO.getValues(keyDocument)).thenReturn(values);
        when(domainSpecificValue.changeSetIs(CHANGE_SET)).thenReturn(true);
        when(domainSpecificValue.getValue()).thenReturn(value);
        when(domainSpecificValue.getPatternStr()).thenReturn(PATTERN);
        when(valueAnalyzerFactory.createValueAnalyzer(domainSpecificValue)).thenReturn(valueAnalyzer);
        when(valueAnalyzer.samePatternAndChangeSetAs(valueDocument)).thenReturn(false);

        mongoDbPersistence.store(KEY, keyValues, CHANGE_SET);

        verify(keyValues).getDomainSpecificValues();
        verify(collectionProvider).getCollection();
        verify(keyDocumentDAO).findKey(KEY);
        List<Document> expectedResult = new ArrayList<>(values);
        Document expectedDocument = new Document();
        expectedDocument.put(RopertyMongoDbAttribute.PATTERN.getName(), PATTERN);
        expectedDocument.put(RopertyMongoDbAttribute.VALUE.getName(), value);
        expectedDocument.put(RopertyMongoDbAttribute.CHANGE_SET.getName(), CHANGE_SET);
        expectedResult.add(expectedDocument);
        verify(keyDocument).put(VALUES.getName(), expectedResult);
        verify(valueDocumentDAO).getValues(keyDocument);
        verify(collection).updateOne(any(Bson.class), eq(new Document("$set", keyDocument)));
        verify(domainSpecificValue).changeSetIs(CHANGE_SET);
        verify(domainSpecificValue).getValue();
        verify(domainSpecificValue).getPatternStr();
        verify(valueAnalyzerFactory).createValueAnalyzer(domainSpecificValue);
        verify(valueAnalyzer).samePatternAndChangeSetAs(valueDocument);
    }

    @Test
    public void storeNewKeyWithChangeSetShouldCreateNewValueDocument() {
        when(keyValues.getDomainSpecificValues()).thenReturn(domainSpecificValues);
        when(keyValues.getDescription()).thenReturn(DESCRIPTION);
        when(collectionProvider.getCollection()).thenReturn(collection);
        when(keyDocumentDAO.findKey(KEY)).thenReturn(null);
        when(domainSpecificValue.changeSetIs(CHANGE_SET)).thenReturn(true);
        when(domainSpecificValue.getValue()).thenReturn(value);
        when(domainSpecificValue.getPatternStr()).thenReturn(PATTERN);
        when(valueAnalyzerFactory.createValueAnalyzer(domainSpecificValue)).thenReturn(valueAnalyzer);

        mongoDbPersistence.store(KEY, keyValues, CHANGE_SET);

        verify(keyValues).getDomainSpecificValues();
        verify(keyValues).getDescription();
        verify(collectionProvider).getCollection();
        verify(keyDocumentDAO).findKey(KEY);
        Document expectedValueDocument = new Document();
        expectedValueDocument.put(RopertyMongoDbAttribute.PATTERN.getName(), PATTERN);
        expectedValueDocument.put(RopertyMongoDbAttribute.CHANGE_SET.getName(), CHANGE_SET);
        expectedValueDocument.put(RopertyMongoDbAttribute.VALUE.getName(), value);
        Document expectedKeyDocument = new Document();
        expectedKeyDocument.put(RopertyMongoDbAttribute.KEY.getName(), KEY);
        expectedKeyDocument.put(RopertyMongoDbAttribute.DESCRIPTION.getName(), DESCRIPTION);
        expectedKeyDocument.put(RopertyMongoDbAttribute.VALUES.getName(), Arrays.asList(expectedValueDocument));
        verify(collection).insertOne(expectedKeyDocument);
        verify(domainSpecificValue).changeSetIs(CHANGE_SET);
        verify(domainSpecificValue).getValue();
        verify(domainSpecificValue).getPatternStr();
        verify(valueAnalyzerFactory).createValueAnalyzer(domainSpecificValue);
    }

    @Test(expected = NullPointerException.class)
    public void removeWithoutKeyShouldFailForKeyValues() {
        mongoDbPersistence.remove(null, (KeyValues) null, null);
    }

    @Test
    public void removeNonExistingKeyShouldDoNothing() {

        mongoDbPersistence.remove(KEY, keyValues, null);

        verify(keyDocumentDAO).findKey(KEY);
        verifyZeroInteractions(keyValuesFactory, domainSpecificValueFactory, collectionProvider, keyDocumentDAO,
            keyDocument, valueDocumentDAO, valueDocument, valueDocumentTransformerFactory, valueDocumentTransformer,
            keyValues, collection, keyBlockFactory, keyBlock, allKeys, domainSpecificValue, valueAnalyzerFactory,
            removeOperationFactory, removeOperation);
    }

    @Test
    public void removeWithoutKeyValuesShouldRemoveKey() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(removeOperationFactory.createRemoveOperation(keyDocument)).thenReturn(removeOperation);

        mongoDbPersistence.remove(KEY, (KeyValues) null, null);

        verify(removeOperationFactory).createRemoveOperation(keyDocument);
        verify(removeOperation).removeKey();
    }

    @Test
    public void removeWithKeyValuesShouldRemoveKeyAndValues() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(removeOperationFactory.createRemoveOperation(keyDocument)).thenReturn(removeOperation);
        when(keyValues.getDomainSpecificValues()).thenReturn(domainSpecificValues);

        mongoDbPersistence.remove(KEY, keyValues, null);

        verify(keyDocumentDAO).findKey(KEY);
        verify(removeOperationFactory).createRemoveOperation(keyDocument);
        verify(keyValues).getDomainSpecificValues();
        verify(removeOperation).removeValue(domainSpecificValue);
        verify(removeOperation).removeKeyIfNoMoreValues();

        verifyZeroInteractions(keyValuesFactory, domainSpecificValueFactory, collectionProvider, keyDocumentDAO,
            keyDocument, valueDocumentDAO, valueDocument, valueDocumentTransformerFactory, valueDocumentTransformer,
            keyValues, collection, keyBlockFactory, keyBlock, allKeys, domainSpecificValue, valueAnalyzerFactory,
            removeOperationFactory, removeOperation);
    }


    @Test
    public void removeWithDomainSpecificValueShouldRemoveKeyAndValue() {
        when(keyDocumentDAO.findKey(KEY)).thenReturn(keyDocument);
        when(removeOperationFactory.createRemoveOperation(keyDocument)).thenReturn(removeOperation);

        mongoDbPersistence.remove(KEY, domainSpecificValue, null);

        verify(keyDocumentDAO).findKey(KEY);
        verify(removeOperationFactory).createRemoveOperation(keyDocument);
        verify(removeOperation).removeValue(domainSpecificValue);
        verify(removeOperation).removeKeyIfNoMoreValues();

        verifyZeroInteractions(keyValuesFactory, domainSpecificValueFactory, collectionProvider, keyDocumentDAO,
            keyDocument, valueDocumentDAO, valueDocument, valueDocumentTransformerFactory, valueDocumentTransformer,
            keyValues, collection, keyBlockFactory, keyBlock, allKeys, domainSpecificValue, valueAnalyzerFactory,
            removeOperationFactory, removeOperation);
    }

    @Test(expected = NullPointerException.class)
    public void removeWithoutKeyShouldFailForDomainSpecificValue() {
        mongoDbPersistence.remove(null, (DomainSpecificValue) null, null);
    }

    @Test(expected = NullPointerException.class)
    public void removeWithoutDomainSpecificValuesShouldFail() {
        mongoDbPersistence.remove(KEY, (DomainSpecificValue) null, null);
    }


}
