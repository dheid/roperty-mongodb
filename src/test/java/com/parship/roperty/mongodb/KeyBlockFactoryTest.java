package com.parship.roperty.mongodb;

import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValuesFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class KeyBlockFactoryTest {

    public static final int NUM_KEYS = 42;

    @InjectMocks
    private KeyBlockFactory keyBlockFactory;

    @Mock
    private KeyValuesFactory keyValuesFactory;

    @Mock
    private DomainSpecificValueFactory domainSpecificValueFactory;

    @Test(expected = IllegalArgumentException.class)
    public void failIfNegativeNumberOfKeys() {
        keyBlockFactory.createKeyBlock(-1, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void failIfNoKeyValuesFactory() {
        keyBlockFactory.createKeyBlock(NUM_KEYS, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void failIfNoDomainSpecificValuesFactory() {
        keyBlockFactory.createKeyBlock(NUM_KEYS, keyValuesFactory, null);
    }

    @Test
    public void createKeyBlock() {
        KeyBlock result = keyBlockFactory.createKeyBlock(NUM_KEYS, keyValuesFactory, domainSpecificValueFactory);
        assertThat(result, not(nullValue()));
    }


}
