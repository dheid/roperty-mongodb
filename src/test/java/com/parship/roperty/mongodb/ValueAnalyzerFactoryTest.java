package com.parship.roperty.mongodb;

import com.parship.roperty.DomainSpecificValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ValueAnalyzerFactoryTest {

    @InjectMocks
    private ValueAnalyzerFactory valueAnalyzerFactory;

    @Mock
    private DomainSpecificValue domainSpecificValue;

    @Test(expected = NullPointerException.class)
    public void failIfNoDomainSpecificValue() {
        valueAnalyzerFactory.createValueAnalyzer(null);
    }

    @Test
    public void createValueAnalyzer() {
        ValueAnalyzer valueAnalyzer = valueAnalyzerFactory.createValueAnalyzer(domainSpecificValue);
        assertThat(valueAnalyzer, not(nullValue()));
    }

}
