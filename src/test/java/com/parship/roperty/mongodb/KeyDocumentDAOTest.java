package com.parship.roperty.mongodb;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertThat;

/**
 * Created by daniel on 30.03.17.
 */
public class KeyDocumentDAOTest {

    private KeyDocumentDAO keyDocumentDAO = new KeyDocumentDAO();

    @Test
    public void returnsEmptyListOnBlankSubstring() throws Exception {
        List<String> keys = keyDocumentDAO.findKeys(" ");
        assertThat(keys, emptyIterable());
    }

    @Test(expected = NullPointerException.class)
    public void failsIfNoCollectionProvider() throws Exception {
        keyDocumentDAO.findKeys("substring");
    }

}
