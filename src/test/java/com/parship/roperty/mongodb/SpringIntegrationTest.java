package com.parship.roperty.mongodb;

import com.mongodb.MongoClient;
import com.parship.roperty.DomainResolver;
import com.parship.roperty.MapBackedDomainResolver;
import com.parship.roperty.Roperty;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class SpringIntegrationTest {

    private static final int MONGO_PORT = 21321;

    private static MongodExecutable mongodExecutable;
    private static MongodProcess mongodProcess;

    @Autowired
    private Roperty roperty;

    @Autowired
    private CollectionProvider collectionProvider;

    @BeforeClass
    public static void startEmbeddedMongoDB() throws IOException {
        IMongodConfig mongodConfig = new MongodConfigBuilder()
            .version(Version.Main.PRODUCTION)
            .net(new Net(MONGO_PORT, Network.localhostIsIPv6()))
            .build();

        MongodStarter starter = MongodStarter.getDefaultInstance();
        mongodExecutable = starter.prepare(mongodConfig);
        mongodProcess = mongodExecutable.start();
    }

    @AfterClass
    public static void stopMongodExecutable() {
        mongodProcess.stop();
        mongodExecutable.stop();
    }

    @Before
    public void configureCollectionProvider() {
        collectionProvider.setMongo(new MongoClient("localhost", MONGO_PORT));
    }

    @Test
    public void setAndGetValue() {

        roperty.addDomains("country", "city");

        roperty.set("address", "Dam", "Some beautiful spot", "Netherlands", "Amsterdam");
        roperty.set("address", "Boulevard du Palais", "Some beautiful spot", "France", "Paris");
        roperty.set("address", "Place Saint-Jean", "Some beautiful spot", "France", "Lyon");
        roperty.set("address", "Trafalgar Square", "Some beautiful spot", "United Kingdom", "London");
        roperty.set("address", "Castlehill", "Some beautiful spot", "United Kingdom", "Edinburgh");

        roperty.reload();

        DomainResolver resolver = new MapBackedDomainResolver().set("country", "United Kingdom").set("city", "Edinburgh");
        String value = roperty.get("address", "defaultValue", resolver); // Castlehill

        assertThat(value, is("Castlehill"));
    }


}
