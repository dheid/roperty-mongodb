package com.parship.roperty.mongodb;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.parship.roperty.DomainSpecificValue;
import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValues;
import com.parship.roperty.KeyValuesFactory;
import com.parship.roperty.Persistence;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.bson.BsonDateTime;
import org.bson.Document;
import org.joda.time.ReadableInstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isBlank;


public class MongoDbPersistence implements Persistence {

    private CollectionProvider collectionProvider;

    private KeyDocumentDAO keyDocumentDAO;

    private ValueDocumentDAO valueDocumentDAO;

    private RemoveOperationFactory removeOperationFactory;

    private ValueDocumentTransformerFactory valueDocumentTransformerFactory;

    private ValueAnalyzerFactory valueAnalyzerFactory;

    private KeyBlockFactory keyBlockFactory;

    @Override
    public KeyValues load(String key, KeyValuesFactory keyValuesFactory, DomainSpecificValueFactory domainSpecificValueFactory) {
        Validate.notBlank(key, "Key must not be empty");
        Validate.notNull(keyValuesFactory, "Key values factory must no be null");
        Validate.notNull(domainSpecificValueFactory, "Domain specific value factory must not be null");
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        Validate.notNull(keyDocumentDAO, "Key document DAO must not be null");

        Document keyDocument = keyDocumentDAO.findKey(key);
        if (keyDocument == null) {
            return null;
        }

        Validate.notNull(valueDocumentDAO, "Value document DAO must not be null");

        List<Document> values = valueDocumentDAO.getValues(keyDocument);
        String description = keyDocument.getString(RopertyMongoDbAttribute.DESCRIPTION.getName());

        Validate.notNull(valueDocumentTransformerFactory, "Value document transformer factory must not be null");

        ValueDocumentTransformer valueDocumentTransformer = valueDocumentTransformerFactory.create(keyValuesFactory, domainSpecificValueFactory);
        return valueDocumentTransformer.transform(values, description);
    }

    @Override
    public Map<String, KeyValues> loadAll(final KeyValuesFactory keyValuesFactory, final DomainSpecificValueFactory domainSpecificValueFactory) {
        Validate.notNull(keyValuesFactory, "Key values factory must no be null");
        Validate.notNull(domainSpecificValueFactory, "Domain specific value factory must not be null");
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        Validate.notNull(keyBlockFactory, "Key block factory must no be null");
        Validate.notNull(keyDocumentDAO, "Key document DAO must no be null");

        long numKeys = collectionProvider.getCollection().count();
        KeyBlock keyBlock = keyBlockFactory.createKeyBlock((int)numKeys, keyValuesFactory, domainSpecificValueFactory);

        FindIterable<Document> keyDocuments = keyDocumentDAO.findAllKeys();
        keyDocuments.forEach(keyBlock);

        return Collections.unmodifiableMap(keyBlock.getResult());
    }

    @Override
    public Map<String, KeyValues> reload(Map<String, KeyValues> keyValuesMap, KeyValuesFactory keyValuesFactory, DomainSpecificValueFactory domainSpecificValueFactory) {
        Validate.notNull(keyValuesMap, "Key values map should not be null");
        Validate.notNull(keyValuesFactory, "Key values factory must no be null");
        Validate.notNull(domainSpecificValueFactory, "Domain specific value factory must not be null");

        int inputSize = keyValuesMap.size();
        if (inputSize > 0) {
            Validate.notNull(keyDocumentDAO, "Key document DAO must not be null");
        }

        Map<String, KeyValues> result = new HashMap<>(inputSize);

        for (Map.Entry<String, KeyValues> entry : keyValuesMap.entrySet()) {
            String key = entry.getKey();
            KeyValues keyValues = entry.getValue();
            Document keyDocument = keyDocumentDAO.findKey(key);
            if (keyDocument != null) {
                List<Document> values = valueDocumentDAO.getValues(keyDocument);
                String description = keyValues.getDescription();
                ValueDocumentTransformer valueDocumentTransformer = valueDocumentTransformerFactory.create(keyValuesFactory, domainSpecificValueFactory);
                result.put(key, valueDocumentTransformer.transform(values, description));
            }
        }

        return Collections.unmodifiableMap(result);
    }

    @Override
    public void store(String key, KeyValues keyValues, String changeSet) {
        Validate.notNull(keyValues, "Key values must not be null");

        Set<DomainSpecificValue> domainSpecificValues = keyValues.getDomainSpecificValues();
        if (domainSpecificValues.isEmpty()) {
            return;
        }

        Validate.notBlank(key, "Key must not be empty");
        Validate.notNull(collectionProvider, "Collection provider must not be null");

        List<Document> valueDocuments;
        Document keyDocument = keyDocumentDAO.findKey(key);
        boolean keyAlreadyExists = keyDocument != null;
        if (keyAlreadyExists) {
            Validate.notNull(valueDocumentDAO, "Value document DAO must not be null");
            valueDocuments = new ArrayList<>(valueDocumentDAO.getValues(keyDocument));
        } else {
            keyDocument = new Document()
                .append(RopertyMongoDbAttribute.KEY.getName(), key)
                .append(RopertyMongoDbAttribute.DESCRIPTION.getName(), keyValues.getDescription());
            valueDocuments = new ArrayList<>(domainSpecificValues.size());
        }

        for (DomainSpecificValue domainSpecificValue : domainSpecificValues) {
            if (domainSpecificValue.changeSetIs(changeSet) || domainSpecificValue.changeSetIs(nullWhenEmpty(changeSet))) {
                ValueAnalyzer valueAnalyzer = valueAnalyzerFactory.createValueAnalyzer(domainSpecificValue);

                boolean valueAlreadyExists = false;
                Object value = convertObject(domainSpecificValue);
                for (Document valueDocument : valueDocuments) {
                    if (valueAnalyzer.samePatternAndChangeSetAs(valueDocument)) {
                        valueAlreadyExists = true;
                        valueDocument.put(RopertyMongoDbAttribute.VALUE.getName(), value);
                        break;
                    }
                }

                if (!valueAlreadyExists) {
                    Document valueDocument = new Document()
                        .append(RopertyMongoDbAttribute.PATTERN.getName(), domainSpecificValue.getPatternStr())
                        .append(RopertyMongoDbAttribute.VALUE.getName(), value);

                    if (!isBlank(changeSet)) {
                        valueDocument.append(RopertyMongoDbAttribute.CHANGE_SET.getName(), changeSet);
                    }

                    valueDocuments.add(valueDocument);
                }
            }
        }

        keyDocument.put(RopertyMongoDbAttribute.VALUES.getName(), valueDocuments);

        MongoCollection<Document> collection = collectionProvider.getCollection();
        if (keyAlreadyExists) {
            collection.updateOne(Filters.eq(RopertyMongoDbAttribute.KEY.getName(), key), new Document("$set", keyDocument));
        } else {
            collection.insertOne(keyDocument);
        }
    }

    private Object convertObject(DomainSpecificValue domainSpecificValue) {
        Object value = domainSpecificValue.getValue();

        if (value == null) {
            return null;
        }

        if (value.getClass().isArray()) {
			List<Object> newValue = new ArrayList<>(((Object[])value).length);
            Collections.addAll(newValue, (Object[])value);
			value = newValue;
		} else if (ReadableInstant.class.isAssignableFrom(value.getClass())) {
			value = new BsonDateTime(((ReadableInstant)value).getMillis());
		}
        return value;
    }

    private static String nullWhenEmpty(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return value;
    }

    @Override
    public void remove(String key, KeyValues keyValues, String changeSet) {
        Validate.notBlank(key, "Key must not be empty");
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        Validate.notNull(keyDocumentDAO, "Key finder must not be null");

        Document keyDocument = keyDocumentDAO.findKey(key);
        if (keyDocument == null) {
            return;
        }

        RemoveOperation removeOperation = removeOperationFactory.createRemoveOperation(keyDocument);

        if (keyValues == null) {
            removeOperation.removeKey();
        } else {
            Set<DomainSpecificValue> domainSpecificValues = keyValues.getDomainSpecificValues();
            for (DomainSpecificValue domainSpecificValue : domainSpecificValues) {
                removeOperation.removeValue(domainSpecificValue);
            }
            removeOperation.removeKeyIfNoMoreValues();
        }

    }

    @Override
    public void remove(String key, DomainSpecificValue domainSpecificValue, String changeSet) {
        Validate.notBlank(key, "Key must not be empty");
        Validate.notNull(domainSpecificValue, "Domain specific value must not be null");
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        Validate.notNull(keyDocumentDAO, "Key finder must not be null");

        Document keyDocument = keyDocumentDAO.findKey(key);
        if (keyDocument == null) {
            return;
        }

        RemoveOperation removeOperation = removeOperationFactory.createRemoveOperation(keyDocument);
        removeOperation.removeValue(domainSpecificValue);
        removeOperation.removeKeyIfNoMoreValues();
    }

    @Override
    public List<String> findKeys(String substring) {

        if (isBlank(substring)) {
            return Collections.emptyList();
        }

        Validate.notNull(collectionProvider, "Collection provider must not be null");
        Validate.notNull(keyDocumentDAO, "Key document DAO must not be null");

        return keyDocumentDAO.findKeys(substring);
    }

    @Override
    public List<String> getAllKeys() {
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        Validate.notNull(keyDocumentDAO, "Key document DAO must not be null");

        return keyDocumentDAO.getAllKeys();
    }

    public void setCollectionProvider(CollectionProvider collectionProvider) {
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        this.collectionProvider = collectionProvider;
    }

    public void setKeyDocumentDAO(KeyDocumentDAO keyDocumentDAO) {
        Validate.notNull(keyDocumentDAO, "Key finder must not be null");
        this.keyDocumentDAO = keyDocumentDAO;
    }

    public void setRemoveOperationFactory(RemoveOperationFactory removeOperationFactory) {
        this.removeOperationFactory = removeOperationFactory;
    }

    public void setValueDocumentDAO(ValueDocumentDAO valueDocumentDAO) {
        this.valueDocumentDAO = valueDocumentDAO;
    }

    public void setKeyBlockFactory(KeyBlockFactory keyBlockFactory) {
        this.keyBlockFactory = keyBlockFactory;
    }

    public void setValueDocumentTransformerFactory(ValueDocumentTransformerFactory valueDocumentTransformerFactory) {
        this.valueDocumentTransformerFactory = valueDocumentTransformerFactory;
    }

    public void setValueAnalyzerFactory(ValueAnalyzerFactory valueAnalyzerFactory) {
        this.valueAnalyzerFactory = valueAnalyzerFactory;
    }
}
