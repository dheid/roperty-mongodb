package com.parship.roperty.mongodb;

import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValuesFactory;
import org.apache.commons.lang3.Validate;

public class KeyBlockFactory {

    private ValueDocumentTransformerFactory valueDocumentTransformerFactory;

    public KeyBlock createKeyBlock(int numKeys, KeyValuesFactory keyValuesFactory, DomainSpecificValueFactory domainSpecificValueFactory) {
        Validate.inclusiveBetween(0L, Integer.MAX_VALUE, numKeys);
        Validate.notNull(keyValuesFactory, "Key values factory must not be null");
        Validate.notNull(domainSpecificValueFactory, "Domain specific value factory must not be null");
        KeyBlock keyBlock = new KeyBlock();
        keyBlock.setNumKeys(numKeys);
        keyBlock.setDomainSpecificValueFactory(domainSpecificValueFactory);
        keyBlock.setKeyValuesFactory(keyValuesFactory);
        keyBlock.setValueDocumentTransformerFactory(valueDocumentTransformerFactory);
        return keyBlock;
    }

    public void setValueDocumentTransformerFactory(ValueDocumentTransformerFactory valueDocumentTransformerFactory) {
        this.valueDocumentTransformerFactory = valueDocumentTransformerFactory;
    }
}
