package com.parship.roperty.mongodb;

import com.mongodb.client.MongoCollection;
import com.parship.roperty.DomainSpecificValue;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class RemoveOperation {

    private CollectionProvider collectionProvider;

    private KeyDocumentDAO keyDocumentDAO;

    private ValueDocumentDAO valueDocumentDAO;

    private int numValues;

    private int numRemoved = 0;

    private Document keyDocument;

    private List<Document> values;

    public void prepare(Document keyDocument) {
        values = new ArrayList<>(valueDocumentDAO.getValues(keyDocument));
        numValues = values.size();
        this.keyDocument = keyDocument;
    }

    public void removeValue(DomainSpecificValue domainSpecificValue) {
        Document removedValue = valueDocumentDAO.removeValue(keyDocument, values, domainSpecificValue);
        if (removedValue != null) {
            numRemoved++;
            values.remove(removedValue);
        }
    }

    public void removeKeyIfNoMoreValues() {
        if (numValues == numRemoved) {
            removeKey();
        }
    }

    public void setCollectionProvider(CollectionProvider collectionProvider) {
        this.collectionProvider = collectionProvider;
    }

    public void setValueDocumentDAO(ValueDocumentDAO valueDocumentDAO) {
        this.valueDocumentDAO = valueDocumentDAO;
    }

    public void setKeyDocumentDAO(KeyDocumentDAO keyDocumentDAO) {
        this.keyDocumentDAO = keyDocumentDAO;
    }

    public void removeKey() {
        MongoCollection<Document> collection = collectionProvider.getCollection();
        String key = keyDocument.getString(RopertyMongoDbAttribute.KEY.getName());
        collection.deleteOne(new Document("key", key));
    }
}
