package com.parship.roperty.mongodb;

import org.bson.Document;

public class RemoveOperationFactory {

    private CollectionProvider collectionProvider;

    private KeyDocumentDAO keyDocumentDAO;

    private ValueDocumentDAO valueDocumentDAO;

    public RemoveOperation createRemoveOperation(Document keyDocument) {
        RemoveOperation removeOperation = new RemoveOperation();
        removeOperation.setCollectionProvider(collectionProvider);
        removeOperation.setKeyDocumentDAO(keyDocumentDAO);
        removeOperation.setValueDocumentDAO(valueDocumentDAO);
        removeOperation.prepare(keyDocument);
        return removeOperation;
    }

    public void setCollectionProvider(CollectionProvider collectionProvider) {
        this.collectionProvider = collectionProvider;
    }

    public void setKeyDocumentDAO(KeyDocumentDAO keyDocumentDAO) {
        this.keyDocumentDAO = keyDocumentDAO;
    }

    public void setValueDocumentDAO(ValueDocumentDAO valueDocumentDAO) {
        this.valueDocumentDAO = valueDocumentDAO;
    }
}
