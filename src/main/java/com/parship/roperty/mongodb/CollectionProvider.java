package com.parship.roperty.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.lang3.Validate;
import org.bson.Document;

public class CollectionProvider {

    private String databaseName;

    private String collectionName;

    private MongoClient mongo;

    public MongoCollection<Document> getCollection() {
        Validate.notNull(mongo, "No mongo client configured");
        Validate.notBlank(databaseName, "Database name must not be blank");
        Validate.notBlank(collectionName, "Collection name must not be blank");
        MongoDatabase database = mongo.getDatabase(databaseName);
        return database.getCollection(collectionName);
    }

    public void setDatabaseName(String databaseName) {
        Validate.notBlank(databaseName, "Database name must not be blank");
        this.databaseName = databaseName;
    }

    public void setMongo(MongoClient mongo) {
        Validate.notNull(mongo, "Mongo client must not be null");
        this.mongo = mongo;
    }

    public void setCollectionName(String collectionName) {
        Validate.notBlank(collectionName, "Collection name must not be blank");
        this.collectionName = collectionName;
    }
}
