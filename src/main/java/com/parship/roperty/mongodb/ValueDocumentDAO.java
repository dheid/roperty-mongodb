package com.parship.roperty.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.parship.roperty.DomainSpecificValue;
import org.apache.commons.lang3.Validate;
import org.bson.Document;

import java.util.Collections;
import java.util.List;

public class ValueDocumentDAO {

    private ValueAnalyzerFactory valueAnalyzerFactory;

    private CollectionProvider collectionProvider;

    public Document removeValue(Document keyDocument, List<Document> values, DomainSpecificValue domainSpecificValue) {
        Validate.notNull(valueAnalyzerFactory, "Value analyzer factory must not be null");
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        Validate.notNull(keyDocument, "Key document must not be null");
        Validate.notNull(values, "Values must not be null");
        Validate.notNull(domainSpecificValue, "Domain specific value must not be null");
        MongoCollection<Document> collection = collectionProvider.getCollection();
        Validate.notNull(collection, "Collection must not be null");

        ValueAnalyzer valueAnalyzer = valueAnalyzerFactory.createValueAnalyzer(domainSpecificValue);
        for (Document value : values) {
            if (valueAnalyzer.samePatternAndChangeSetAs(value)) {
                BasicDBObject update = new BasicDBObject(RopertyMongoDbAttribute.VALUES.getName(), value);
                collection.updateOne(keyDocument, new BasicDBObject("$pull", update));
                return value;
            }
        }
        return null;
    }

    public List<Document> getValues(Document keyDocument) {
        Validate.notNull(keyDocument, "Key document must not be null");
        return Collections.unmodifiableList((List<Document>) keyDocument.get(RopertyMongoDbAttribute.VALUES.getName()));
    }

    public void setCollectionProvider(CollectionProvider collectionProvider) {
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        this.collectionProvider = collectionProvider;
    }

    public void setValueAnalyzerFactory(ValueAnalyzerFactory valueAnalyzerFactory) {
        this.valueAnalyzerFactory = valueAnalyzerFactory;
    }
}
