package com.parship.roperty.mongodb;

public class MongoDbPersistenceFactory {

    public LazyMongoDbPersistence createLazyPersistence(CollectionProvider collectionProvider) {
        return initialize(new LazyMongoDbPersistence(), collectionProvider);
    }

    public MongoDbPersistence createPersistence(CollectionProvider collectionProvider) {
        return initialize(new MongoDbPersistence(), collectionProvider);
    }

    private <T extends MongoDbPersistence> T initialize(T mongoDbPersistence, CollectionProvider collectionProvider) {
        ValueAnalyzerFactory valueAnalyzerFactory = new ValueAnalyzerFactory();

        KeyDocumentDAO keyDocumentDAO = new KeyDocumentDAO();
        keyDocumentDAO.setCollectionProvider(collectionProvider);

        ValueDocumentDAO valueDocumentDAO = new ValueDocumentDAO();
        valueDocumentDAO.setCollectionProvider(collectionProvider);
        valueDocumentDAO.setValueAnalyzerFactory(valueAnalyzerFactory);

        RemoveOperationFactory removeOperationFactory = new RemoveOperationFactory();
        removeOperationFactory.setCollectionProvider(collectionProvider);
        removeOperationFactory.setKeyDocumentDAO(keyDocumentDAO);
        removeOperationFactory.setValueDocumentDAO(valueDocumentDAO);

        ValueDocumentTransformerFactory valueDocumentTransformerFactory = new ValueDocumentTransformerFactory();

        KeyBlockFactory keyBlockFactory = new KeyBlockFactory();
        keyBlockFactory.setValueDocumentTransformerFactory(valueDocumentTransformerFactory);

        mongoDbPersistence.setCollectionProvider(collectionProvider);
        mongoDbPersistence.setKeyDocumentDAO(keyDocumentDAO);
        mongoDbPersistence.setValueDocumentDAO(valueDocumentDAO);
        mongoDbPersistence.setKeyBlockFactory(keyBlockFactory);
        mongoDbPersistence.setRemoveOperationFactory(removeOperationFactory);
        mongoDbPersistence.setValueDocumentTransformerFactory(valueDocumentTransformerFactory);
        mongoDbPersistence.setValueAnalyzerFactory(valueAnalyzerFactory);

        return mongoDbPersistence;
    }
}
