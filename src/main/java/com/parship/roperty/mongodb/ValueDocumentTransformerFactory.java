package com.parship.roperty.mongodb;

import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValuesFactory;

public class ValueDocumentTransformerFactory {

    public ValueDocumentTransformer create(KeyValuesFactory keyValuesFactory, DomainSpecificValueFactory domainSpecificValueFactory) {
        ValueDocumentTransformer valueDocumentTransformer = new ValueDocumentTransformer();
        valueDocumentTransformer.setKeyValuesFactory(keyValuesFactory);
        valueDocumentTransformer.setDomainSpecificValueFactory(domainSpecificValueFactory);
        return valueDocumentTransformer;
    }

}
