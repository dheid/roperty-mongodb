package com.parship.roperty.mongodb;

public enum RopertyMongoDbAttribute {
    PATTERN("pattern"), VALUE("value"), KEY("key"), DESCRIPTION("description"), CHANGE_SET("change_set"), VALUES("values");

    private String name;

    private RopertyMongoDbAttribute(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
