package com.parship.roperty.mongodb;

import com.mongodb.Block;
import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValues;
import com.parship.roperty.KeyValuesFactory;
import org.bson.Document;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeyBlock implements Block<Document> {

    private KeyValuesFactory keyValuesFactory;
    private DomainSpecificValueFactory domainSpecificValueFactory;
    private Map<String, KeyValues> result;
    private ValueDocumentTransformerFactory valueDocumentTransformerFactory;

    @Override
    public void apply(Document document) {
        String key = document.getString(RopertyMongoDbAttribute.KEY.getName());
        List<Document> values = (List<Document>) document.get(RopertyMongoDbAttribute.VALUES.getName());
        String description = document.getString(RopertyMongoDbAttribute.DESCRIPTION.getName());
        ValueDocumentTransformer valueDocumentTransformer = valueDocumentTransformerFactory.create(keyValuesFactory, domainSpecificValueFactory);
        result.put(key, valueDocumentTransformer.transform(values, description));
    }

    public void setKeyValuesFactory(KeyValuesFactory keyValuesFactory) {
        this.keyValuesFactory = keyValuesFactory;
    }

    public void setDomainSpecificValueFactory(DomainSpecificValueFactory domainSpecificValueFactory) {
        this.domainSpecificValueFactory = domainSpecificValueFactory;
    }

    public void setValueDocumentTransformerFactory(ValueDocumentTransformerFactory valueDocumentTransformerFactory) {
        this.valueDocumentTransformerFactory = valueDocumentTransformerFactory;
    }

    public void setNumKeys(int numKeys) {
        result = new HashMap<>(numKeys);
    }

    public Map<String, KeyValues> getResult() {
        return result;
    }
}
