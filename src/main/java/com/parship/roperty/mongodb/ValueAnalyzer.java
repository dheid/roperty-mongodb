package com.parship.roperty.mongodb;

import java.util.Objects;

import com.parship.roperty.DomainSpecificValue;

import org.apache.commons.lang3.Validate;
import org.bson.Document;

import static org.apache.commons.lang3.StringUtils.isEmpty;


public class ValueAnalyzer {

    private DomainSpecificValue domainSpecificValue;

    public void setDomainSpecificValue(DomainSpecificValue domainSpecificValue) {
        Validate.notNull(domainSpecificValue, "Domain specific value must not be null");
        this.domainSpecificValue = domainSpecificValue;
    }

    public boolean samePatternAndChangeSetAs(Document valueDocument) {
        Validate.notNull(valueDocument, "Value document must not be null");
        Validate.notNull(domainSpecificValue, "Domain specific value must not be null");

        String newPatternStr = domainSpecificValue.getPatternStr();
        String oldPatternStr = valueDocument.getString(RopertyMongoDbAttribute.PATTERN.getName());

        String newChangeSet = nullIfEmpty(domainSpecificValue.getChangeSet());
        String oldChangeSet = nullIfEmpty(valueDocument.getString(RopertyMongoDbAttribute.CHANGE_SET.getName()));

        return Objects.equals(newPatternStr, oldPatternStr) && Objects.equals(newChangeSet, oldChangeSet);
    }

    private static String nullIfEmpty(String value) {
        if (isEmpty(value)) {
            return null;
        }
        return value;
    }

}
