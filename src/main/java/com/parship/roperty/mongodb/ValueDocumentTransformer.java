package com.parship.roperty.mongodb;

import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValues;
import com.parship.roperty.KeyValuesFactory;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import java.util.List;

public class ValueDocumentTransformer {

    private KeyValuesFactory keyValuesFactory;

    private DomainSpecificValueFactory domainSpecificValueFactory;

    public KeyValues transform(List<Document> values, String description) {
        KeyValues keyValues = keyValuesFactory.create(domainSpecificValueFactory);
        keyValues.setDescription(description);

        for (Document valueDocument : values) {
            String pattern = valueDocument.getString(RopertyMongoDbAttribute.PATTERN.getName());
            String[] domainKeyParts;
            if (StringUtils.isEmpty(pattern)) {
                domainKeyParts = new String[0];
            } else {
                domainKeyParts = pattern.split("\\|");
            }
            Object value = valueDocument.get(RopertyMongoDbAttribute.VALUE.getName());
            String changeSet = valueDocument.getString(RopertyMongoDbAttribute.CHANGE_SET.getName());
            if (changeSet == null) {
                keyValues.put(value, domainKeyParts);
            } else {
                keyValues.putWithChangeSet(changeSet, value, domainKeyParts);
            }
        }

        return keyValues;
    }

    public void setKeyValuesFactory(KeyValuesFactory keyValuesFactory) {
        this.keyValuesFactory = keyValuesFactory;
    }

    public void setDomainSpecificValueFactory(DomainSpecificValueFactory domainSpecificValueFactory) {
        this.domainSpecificValueFactory = domainSpecificValueFactory;
    }
}
