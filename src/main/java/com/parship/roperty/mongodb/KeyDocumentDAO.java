package com.parship.roperty.mongodb;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import org.apache.commons.lang3.Validate;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class KeyDocumentDAO {

    private CollectionProvider collectionProvider;

    public Document findKey(String key) {
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        MongoCollection<Document> collection = collectionProvider.getCollection();
        Validate.notNull(collection, "Collection must not be null");
        return collection.find(eq(RopertyMongoDbAttribute.KEY.getName(), key)).first();
    }

    public void setCollectionProvider(CollectionProvider collectionProvider) {
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        this.collectionProvider = collectionProvider;
    }

    public FindIterable<Document> findAllKeys() {
        Validate.notNull(collectionProvider, "Collection provider must not be null");
        MongoCollection<Document> collection = collectionProvider.getCollection();
        Validate.notNull(collection, "Collection must not be null");
        return collection.find();
    }

    public List<String> findKeys(String substring) {

        if (isBlank(substring)) {
            return Collections.emptyList();
        }

        Validate.notNull(collectionProvider, "Collection provider must not be null");

        String quotedSubstring = Pattern.quote(substring);
        String regex = "(?i).*" + quotedSubstring + ".*";

        try {
            Pattern pattern = Pattern.compile(regex);
            Bson filter = Filters.regex(RopertyMongoDbAttribute.KEY.getName(), pattern);
            return getKeys(filter);
        } catch (PatternSyntaxException exception) {
            throw new IllegalArgumentException(String.format("Pattern '%s' is invalid", regex), exception);
        }

    }

    public List<String> getAllKeys() {
        return getKeys(new BsonDocument());
    }

    private List<String> getKeys(Bson filter) {
        MongoCollection<Document> collection = collectionProvider.getCollection();
        List<String> result = new ArrayList<>();
        collection
            .find(filter)
            .projection(fields(Projections.include(RopertyMongoDbAttribute.KEY.getName()), excludeId()))
            .forEach((Block<Document>) doc -> result.add(doc.getString(RopertyMongoDbAttribute.KEY.getName())));
        return result;
    }

}
