package com.parship.roperty.mongodb;

import com.parship.roperty.DomainSpecificValue;
import org.apache.commons.lang3.Validate;

public class ValueAnalyzerFactory {
    public ValueAnalyzer createValueAnalyzer(DomainSpecificValue domainSpecificValue) {
        Validate.notNull(domainSpecificValue, "Domain specific value must not be null");
        ValueAnalyzer valueAnalyzer = new ValueAnalyzer();
        valueAnalyzer.setDomainSpecificValue(domainSpecificValue);
        return valueAnalyzer;
    }
}
