# Roperty MongoDB

This Java library is a MongoDB persistence implementation for the advanced property management and retrieval system Roperty (https://gitlab.com/parship/roperty). Use it to store Roperty-managed values inside the fast and highly available MongoDB document database.

I also provide a JPA persistence layer for Roperty. It can be found at https://gitlab.com/dheid/roperty-jpa

## Continuous Integration

[![Linux/Mac Build Status](https://secure.travis-ci.org/dheid/roperty-mongodb.png)](http://travis-ci.org/dheid/roperty-mongodb)
[![Windows Build Status](https://img.shields.io/appveyor/ci/dheid/roperty-mongodb/master.svg?label=windows)](https://ci.appveyor.com/project/dheid/roperty-mongodb/branch/master)
[![Coverage Status](https://coveralls.io/repos/dheid/roperty-mongodb/badge.svg?branch=master&service=gitlab)](https://coveralls.io/github/dheid/roperty-mongodb?branch=master)
[![Maven Central](https://img.shields.io/maven-central/v/io.github.dheid/roperty-mongodb.svg?maxAge=2592000)](http://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22io.github.dheid%22%20AND%20a%3A%22roperty-mongodb%22)

## Installation

In order to use the Roperty MongoDB in your application, you need a running MongoDB server instance that is accessible over network. You'll find a detailed instruction on how to install MongoDB here: https://docs.mongodb.com/manual/installation/

### MongoDB server
  
To install the MongoDB server daemon and the command-line client on Debian and Ubuntu systems, simply execute

```bash
sudo apt-get install mongodb
```

### Indexes on MongoDB collection

Indexes speed up the Roperty key access. The file create_index_example.js contains an example on how to create these indexes:
 
```javascript
db.properties.createIndex( { key: 1 }, { unique: true } );
```

## Dependency management

Using Maven, your application needs the following dependencies:

```xml
<dependency>
   <groupId>com.parship</groupId>
   <artifactId>roperty-mongodb</artifactId>
   <version>1.1</version>
</dependency>
```

## Spring application context configuration

In most cases, using Spring is the preferred way to initialize the MongoDB persistence for Roperty. Below is an example XML-based application context configuration for Spring. Replace the constructor arguments ```localhost``` (host) and ```27017``` (port) with your setup:
  
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="mongo" class="com.mongodb.MongoClient">
      <constructor-arg type="java.lang.String" value="localhost" />
      <constructor-arg type="int" value="27017" />
    </bean>

    <bean id="roperty" class="com.parship.roperty.RopertyImpl">
        <property name="persistence" ref="mongoDbPersistence"/>
    </bean>

    <bean id="mongoDbPersistence" class="com.parship.roperty.persistence.mongo.MongoDbPersistence">
        <property name="collectionProvider" ref="collectionProvider"/>
        <property name="keyDocumentDAO" ref="keyDocumentDAO"/>
        <property name="valueDocumentDAO" ref="valueDocumentDAO"/>
        <property name="keyBlockFactory" ref="keyBlockFactory"/>
        <property name="removeOperationFactory" ref="removeOperationFactory"/>
        <property name="valueDocumentTransformerFactory" ref="valueDocumentTransformerFactory"/>
        <property name="valueAnalyzerFactory" ref="valueAnalyzerFactory"/>
    </bean>

    <bean id="collectionProvider" class="com.parship.roperty.persistence.mongo.CollectionProvider">
        <property name="mongo" ref="mongo"
        <property name="databaseName" value="roperty_database"/>
        <property name="collectionName" value="roperty_collection"/>
    </bean>

    <bean id="keyDocumentDAO" class="com.parship.roperty.persistence.mongo.KeyDocumentDAO">
        <property name="collectionProvider" ref="collectionProvider"/>
    </bean>

    <bean id="valueDocumentDAO" class="com.parship.roperty.persistence.mongo.ValueDocumentDAO">
        <property name="collectionProvider" ref="collectionProvider"/>
        <property name="valueAnalyzerFactory" ref="valueAnalyzerFactory"/>
    </bean>

    <bean id="keyBlockFactory" class="com.parship.roperty.persistence.mongo.KeyBlockFactory"/>

    <bean id="removeOperationFactory" class="com.parship.roperty.persistence.mongo.RemoveOperationFactory">
        <property name="collectionProvider" ref="collectionProvider"/>
        <property name="keyDocumentDAO" ref="keyDocumentDAO"/>
        <property name="valueDocumentDAO" ref="valueDocumentDAO"/>
    </bean>

    <bean id="valueDocumentTransformerFactory"
          class="com.parship.roperty.persistence.mongo.ValueDocumentTransformerFactory"/>

    <bean id="valueAnalyzerFactory"
          class="com.parship.roperty.persistence.mongo.ValueAnalyzerFactory"/>

</beans>
```

## Usage

To define and access properties inside your application, inject the MongoDB persistence provided by this library into Roperty. Here is a quick example on how to use Roperty: 

### Roperty initialization using MongoDB persistence

By having a look at the interface ```Roperty``` you shortly see how you use the component to retrieve properties. The concrete implementation is given be the class ```RopertyImpl```. Roperty holds every value in a (volatile) cache. If you want these values to be stored even after a restart of your application, you need to inject a persistence implementation. That's where the MongoDB persistence is needed.

The connection to MongoDB is established using the class ```CollectionProvider```. You need to inject a ```MongoClient``` object, created with your environment specific parameters.
 
Objects of the class ```MongoDbPersistence``` implement the Roperty-specific interface ```Persistence```. It will be injected into the Roperty implementation and used by it on CRUD operations. To create a ```MongoDbPersistence``` object, create a new ```MongoDbPersistenceFactory``` object.

```java

MongoClient mongo = new MongoClient();
// add connection specific parameters to the mongo object

CollectionProvider collectionProvider = new CollectionProvider();
collectionProvider.setMongo(mongo);
collectionProvider.setDatabaseName("roperty");
collectionProvider.setCollectionName("properties");

MongoDbPersistenceFactory mongoDbPersistenceFactory = new MongoDbPersistenceFactory();
MongoDbPersistence mongoDbPersistence = mongoDbPersistenceFactory.createLazyPersistence(collectionProvider);

// ... here comes some initialization of the persistence ... 
Roperty roperty = new RopertyImpl();
roperty.setPersistence(mongoDbPersistence);
```


I recommend to use the lazy persistence provided by the class ```LazyMongoDbPersistence```. It has the advantage, that the values won't be loaded all together at once into the cache on Roperty initialization.

### Defining domains

To use Roperty first you need to initialize Roperty with some domains by using the method ```roperty.add(...)```. You can imagine them as dimensions. We use countries and within the countries different cities:
  
```java
roperty.addDomains("country", "city");
```

### Storing properties

To fill Roperty and the underlying persistence (MongoDB or JPA) with properties, you can either set them before accessing them or do both by using ```roperty.getOrDefine(...)```. We will define some properties first and access them later:

```java
roperty.set("address", "Dam", "Some beautiful spot", "Netherlands", "Amsterdam");
roperty.set("address", "Boulevard du Palais", "Some beautiful spot", "France", "Paris");
roperty.set("address", "Place Saint-Jean", "Some beautiful spot", "France", "Lyon");
roperty.set("address", "Trafalgar Square", "Some beautiful spot", "United Kingdom", "London");
roperty.set("address", "Castlehill", "Some beautiful spot", "United Kingdom", "Edinburgh");
```

### Accessing property values

The properties will be stored in the cache and in the persistence. Your application may access them later by using the method ```roperty.get(...)```. To access the value for the United Kingdom and Edinburgh, we need a configured ```DomainResolver``` and the corresponding key ```address```: 

```java
DomainResolver resolver = new MapBackedDomainResolver().set("country", "United Kingdom").set("city", "Edinburgh");
String value = roperty.get("address", "defaultValue", resolver); // Castlehill
```

## License

This project is licensed under the Apache Software License, Version 2.0. See file LICENSE for more information.

## Contact

You can use the GitHub contact functions to communicate about this project.
